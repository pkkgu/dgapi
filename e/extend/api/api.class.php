<?php
class api {
	public $empire , $public_r , $ecms_config , $dbtbpre , $level_r, $class_r;
	public $SQL;
	public $mid; //模型ID
	public $tbname; //表名
	public $table;  //表全名
	public $classid = 0;
	public $errCode = 0;
	public $errMsg = 'success';
	public $printType = 'return'; //return=原样返回|json=返回json|echo=输出json
	public function __construct(){
		global $public_r, $empire, $dbtbpre, $ecms_config , $level_r, $class_r;
		$this->empire = $empire;
		$this->public_r = $public_r;
		$this->dbtbpre = $dbtbpre;
		$this->ecms_config = $ecms_config;
		$this->level_r = $level_r;
		$this->class_r = $class_r;
	}
	
	public function __get($name){
		return false;
	}
	
	public function __set($name , $value){
		return false;
	}
	
	/* load */
	public function load($name = '' , $config = array()){
		$file = './_class/' . $name . '.class.php';
		if(!is_file($file)){
			$this->error($name.'.class.php 不存在');
		}else{
			require_once($file);
		}
		$cname = 'api_'.$name;
		if(!class_exists($cname)){
			$this->error('api_'.$name.' 未定义');
		}
		$config = is_array($config) ? $config : array();
		return @new $cname($config);
	}
	
	public function import($name='' , $model='' , $assign = array()){
		if(is_array($model)){
			$assign = $model;
			$model = '';
		}
		$controller = $this->controller($name , $model);
		if(!empty($assign)){
			foreach($assign as $key=>$val){
				$$key = $val;
			}	
		}
		$api = $this;
		include($controller);
	}
	
	/* get */
	public function controller($name = '' , $model = ''){
		$model = $model !== '' ? $model : api_m;
		return './'.$model.'/'.$name.'.php';
	}
	
	/* cache */
	public function cache($name , $fn , $time = 0 , $format = true){
		$time = (int)$time;
		$filename = md5($name . $this->cachehash);
		$filepath = './_cache/'.$filename;
		if(is_bool($fn) && true === $fn){
			@unlink($filepath);
		}else{
			$mtime = is_file($filepath) ? @filemtime($filepath) : false;
			if($mtime && time() - $mtime <= $time){
				$data = @file_get_contents($filepath);
				return $format ? unserialize($data) : $data;
			}else{
				if(is_object($fn)){
					$data = @$fn();
					@file_put_contents($filepath , $format ? serialize($data) : $data);
					return $data;
				}else{
					return false;
				}
			}
		}
	}
	
	/* param */
	public function get($name , $default = '' , $fn = 'trim'){
		$value = isset($_GET[$name]) ? $_GET[$name] : $default;
		return !empty($fn) && function_exists($fn) ? $fn($value) : $value;
	}
	
	public function post($name , $default = '' , $fn = 'trim'){
		$value = isset($_POST[$name]) ? $_POST[$name] : $default;
		return !empty($fn) && function_exists($fn) ? $fn($value) : $value;
	}
	
	public function param($name , $default = '' , $fn = 'trim'){
		$value = isset($_GET[$name]) ? $_GET[$name] : (isset($_POST[$name]) ? $_POST[$name] : $default);
		return !empty($fn) && function_exists($fn) ? $fn($value) : $value;
	}
	
	public function input($name = '' , $default = '' , $fn = 'trim'){
		$input = json_decode(file_get_contents('php://input') , true);
		$input = !empty($input) ? $input : array();
		if(empty($name)){
			return $input;
		}else if(!empty($input)){
			$value = isset($input[$name]) ? $input[$name] : '';
			return !empty($fn) && function_exists($fn) ? $fn($value) : $value;	
		}else{
			return $this->param($name , $default , $fn);
		}
	}
	
	/* output */
	public function show($str , $type = 'text/html' , $charset='utf-8'){
		header('Content-Type: '.$type.'; charset='.$charset);
		exit($str);
	}
	
	public function error($str , $code = 404 , $type = 'text/html' , $charset='utf-8'){
		$this->send_http_status($code);
		$this->show($str , $type , $charset);
	}

	public function json($arr , $options = 0){
		$json = is_array($arr) ? json_encode($arr , $options) : trim($arr);
		$this->show($json , 'application/json');
	}
	
	public function jsonp($arr , $cb = 'callback' , $options = 0){
		$json = is_array($arr) ? json_encode($arr , $options) : trim($arr);
		$cb = $cb ? $cb : 'callback';
		$json = $cb.'('.$json.');';
		$this->show($json , 'application/json');
	}
	
	/* database */
	public function execute($sql='', $exit=true){
		return $exit ? $this->empire->query($sql) : $this->empire->query1($sql);
	}
	
	public function insert($table='', $data=array()){
		if(!$this->tbname($table)) return false;
		if(empty($data) || !is_array($data)) return $this->callback(100008);
		$field = "";
		$value = "";
		foreach($data as $f=>$v){
			$field .= "," . $f;
			$value .= ",'" . RepPostStr($v) ."'";
		}
		$field = substr($field , 1);
		$value = substr($value , 1);
		
		$sql = "insert into {$this->table} ({$field}) values ({$value});";
		$this->SQL = $sql;
		$res = $this->execute($sql);
		return $res ? $this->callback(0, '', array('id'=>$this->empire->lastid())) : $this->callback(100009);
	}
	
	public function update($table, $data , $where){
		if(!$this->tbname($table)) return false;
		if(!$where) return $this->callback(100004);
		if(!$data || (!is_string($data) && !is_array($data))) return $this->callback(100007);
		
		if(is_string($data)){
			$setField = $data;
		}else{
			$setField = "";
			foreach($data as $f=>$v){
				$v = is_array($v) ? $v[0] : $v; 
				$setField .= ",{$f}='{$v}'";
			}
			$setField = substr($setField , 1);
		}
		$sql = "update {$this->table} set {$setField} where {$where}";
		$this->SQL = $sql;
		$res = $this->execute($sql);
		return $this->callback($res ? 0 : 100010);
	}
	
	public function select($table='', $field='*', $where='1', $limit=20, $page=1, $orderby=''){
		if(empty($table)){
			return false;
		}
		$arr = array(
			'table' => '',
			'field' => '*',
			'where' => '1',
			'limit' => 20,
			'page' => 1,
			'orderby' => ''
		);
		$paramType = 0;
		if(is_array($table)){
			$paramType = 1;
			$arr = array_merge($arr , $table);
		}else if(is_array($field)){
			$paramType = 1;
			$arr = array_merge($arr , $field);
			$arr['table'] = $table;
		}
		if($paramType){
			$table = $arr['table'];
			$field = $arr['field'];
			$where = $arr['where'];
			$limit = $arr['limit'];
			$page = $arr['page'];
			$orderby = $arr['orderby'];
		}
		$page = (int)$page;
		$limit = (int)$limit;
		$page = $page > 0 ? $page : 1;
		$limit = $limit > 0 ? $limit : 10;
		$limit = $limit < 1000 ? $limit : 1000;
		$offset = ($page-1) * $limit;
		
		$table = $this->dbtbpre . $table;
		$orderby = $orderby ? 'order by '.$orderby : '';
		$sql = "select {$field} from {$table} where {$where} {$orderby} limit {$offset},{$limit};";
		return $this->query($sql , false);
	}
	
	public function delete($table, $where){
		if(!$this->tbname($table)) return false;
		if(!$where) return $this->callback(100004);
		
		$sql = "delete from {$this->table} where {$where};";
		$this->SQL = $sql;
		$res = $this->execute($sql);
		return $this->callback($res ? 0 : 100011);
	}
	
	public function query($sql='', $exit=false){
		$data = $this->execute($sql , $exit);
		if(false === $data) return false;
		$res = array();
		$bqno = 0;
		while($r = $this->empire->fetch($data)){
			++$bqno;
			$res[] = $this->formatData($r, $bqno);
		}
		return empty($res) ? $this->callback(100006) : $res;
	}
	
	public function one($sql){
		if(!$sql) return $this->callback(100003);
		
		$r = $this->empire->fetch1($sql);
		return $this->formatData($r);
	}
	
	public function total($table, $where){
		if(!$this->tbname($table)) return false;
		if(!$where) return $this->callback(100004);
		$sql = "select count(*) as total from {$this->table} where ".$where;
		$res = $this->empire->gettotal($sql);
		return $this->callback(0 ,'' ,array('count'=>$res));
	}

	private function send_http_status($code) {
		static $_status = array(
			// Informational 1xx
			100 => 'Continue',
			101 => 'Switching Protocols',
			// Success 2xx
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			// Redirection 3xx
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Moved Temporarily ',  // 1.1
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			// 306 is deprecated but reserved
			307 => 'Temporary Redirect',
			// Client Error 4xx
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			// Server Error 5xx
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			509 => 'Bandwidth Limit Exceeded'
		);
		if(isset($_status[$code])) {
			header('HTTP/1.1 '.$code.' '.$_status[$code]);
			// 确保FastCGI模式下正常
			header('Status:'.$code.' '.$_status[$code]);
		}
	}
	
	/**
	 * 获取栏目ID
	 * @param string $classid     栏目ID或栏目目录(栏目目录，因某些项目绑定的栏目可能会变更，另外栏目目录易记，不用再去查询栏目ID)
	 * @return bool|int $classid  返回栏目ID
	 */
	public function classid($classid){
		// 判断数字栏目ID是否存在
		if(is_numeric($classid)){
			$classid = array_key_exists($classid, $this->class_r) ? $classid : 0;
		}
		// 根据栏目目录获取栏目ID
		else if($classid){
			$classid = array_search($classid, array_column($this->class_r, 'classpath'));
		}
		if(!$classid) {
			$this->classid = 0;
			return $this->callback(100002, $classid);
		}
		$this->classid = $classid;
		return $this->classid;
	}
	
	/**
	 * 获取表名
	 * @param string $table 表名-->栏目ID-->栏目目录 (优先级同上)
	 * 注：$table为表名时：以enews开头是判断为非栏目表名（例如会员主表:enewsmember）
	 * @return bool|string $tbname 返回表全名
	 */
	public function tbname($table){
		$this->classid = 0;
		// 判断是否是非栏目表名
		if(substr($table, 0, 5) == 'enews'){
			$this->classid = 0;
			$this->tbname = $table;
			$this->table = $this->dbtbpre.$table;
			return $this->table;
		}
		// 判断是否是栏目表名
		
		$tbname = in_array($table, array_column($this->class_r, 'tbname'));
		if($tbname){
			$this->classid = 0;
			$this->tbname = $table;
			$this->table = $this->dbtbpre.'ecms_'.$table;
			return $this->table;
		}
		// 判断是否是栏目ID或栏目目录，再获取表名
		if(!$this->classid($table)) return false;
		
		$tbname = $this->class_r[$this->classid]['tbname'];
		$this->mid = (int)$this->class_r[$this->classid]['modid'];
		if(!$tbname) {
			$this->tbname = '';
			return $this->callback(100001, $table);
		}
		$this->tbname = $tbname;
		$this->table = $this->dbtbpre.'ecms_'.$tbname;
		return $this->table;
	}
	
	/**
	 * 精简版管理栏目信息 （目前只能管理已审核信息）
	 * 
	 * @param array $add 栏目ID或栏目目录 (必须包含$add['classid'])
	 * @return bool|int  成功返回信息ID
	 * 
	 * 添加
	 * $add = array();
	 * $add['classid'] = 28; //必须
	 * $add['title'] = '测试标题1';   //必须
	 * 
	 * 编辑
	 * $add = array();
	 * $add['id'] = 1;   //修改时不能为空
	 * $add['ecms'] = 1; //0添加|1修改|2删除
	 * $add['classid'] = 8; //必须
	 * $add['title'] = '测试标题2';   //必须
	 *
	 */
	public function addInfor($add){
		if(!$this->tbname($add['classid'])) return false;
		$add['checked'] = 1;//isset($add['checked']) ? (int)$add['checked'] : 1;
		if($add['ecms']==0) // 添加
		{
			//索引表
			$fir = $this->ReturnQAddinfoF($add,2);
			$sql0 = $this->execute("insert into {$this->table}_index(".$fir[6].") values(".$fir[7].")");
			//信息ID
			$add['id'] = $this->empire->lastid();
			// 返回字段
			$fv = $this->ReturnQAddinfoF($add,0);
			// 表名
			$tbname = $this->table.($add['checked'] ? '' : '_check');
			// 主表
			$sql1 = $this->execute("insert into {$tbname}(".$fv[0].") values(".$fv[1].");");
			// 副表
			$sql2 = $this->execute("insert into {$tbname}_data_1(".$fv[2].") values(".$fv[3].");");
			//更新栏目信息数
			AddClassInfos($this->classid,'+1','+1',$add['checked']);
			//更新新信息数
			DoUpdateAddDataNum('info',$this->class_r[$this->classid]['tid'],1);
			return $this->callback(0,'',$add);
		}
		else if($add['ecms']==1) // 修改
		{
			//判断信息ID是否正确
			$add['id'] = (int)$add['id'];
			if(!$add['id']) return $this->callback(100013);
			$r = $this->one("select id,checked from {$this->table}_index where id='$add[id]' limit 1");
			if(!$r['id']) return $this->callback(100014);
			
			// 索引表
			$fir = $this->ReturnQAddinfoF($add,3);
			$sql1=$this->execute("update {$this->table}_index set {$fir[6]} where id='$add[id]'");
			
			// 表名
			$tbname = $this->table.($r['checked'] ? '' : '_check');
			
			// 字段默认值
			$r1 = $this->one("select id,stb,restb,titleurl from {$tbname} where id='$add[id]' limit 1");
			$add['stb'] = $r1['stb'];
			$add['restb'] = $r1['restb'];
			$add['titleurl'] = $r1['titleurl'];
			$fv = $this->ReturnQAddinfoF($add,1);
			// 主表
			$sql1=$this->execute("update {$tbname} set {$fv[0]} where id='$add[id]'");
			// 副表
			$sql2=$this->execute("update {$tbname}_data_{$add['stb']} set {$fv[3]} where id='$add[id]'");
			
			//未审核信息互转
			if($add['checked']!=$r['checked'])
			{
				echo 'h';
				$this->MoveCheckInfoData($this->table, $r['checked'], $r['stb'], "id='$add[id]'");
				echo 'i';
				//更新栏目信息数
				AddClassInfos($this->classid,'',$r['checked'] ? '+1' : '-1');
				echo 'j';
			}
			return $this->callback(0,'',$add);
		}
		return $this->callback(100015);
	}
	
	/**
	 * 信息已审核未审核表切换
	 * @param string  $table    主表表名
	 * @param int     $checked  原信息审核状态
	 * @param int     $stb      原信息副表ID
	 * @param string  $where    条件一般为(id='$id')
	 * @return bool true
	 */
	private function MoveCheckInfoData($table, $checked, $stb, $where){
		if(empty($checked))
		{
			$ytbname = $table.'_check';
			$ydatatbname = $table.'_check_data';
			$ntbname = $table;
			$ndatatbname = $table.'_data_'.$stb;
		}
		else
		{
			$ytbname = $table;
			$ydatatbname = $table.'_data_'.$stb;
			$ntbname = $table.'_check';
			$ndatatbname = $table.'_check_data';
		}
		$this->execute("replace into ".$ntbname." select * from ".$ytbname." where ".$where);
		$this->execute("replace into ".$ndatatbname." select * from ".$ydatatbname." where ".$where);
		//删除原表
		$this->execute("delete from ".$ytbname." where ".$where);
		$this->execute("delete from ".$ydatatbname." where ".$where);
		return false;
	}	
	
	/**
	 * 返回索引、主表、附表字段和值
	 * @param array  $add   数据
	 * @param int    $ecms  0添加|1编辑|2主表添加|3主表编辑
	 * @return bool|array
	 */
	private function ReturnQAddinfoF($add, $ecms=0){
		$time = time();
		$add['title']      = $add['title'] ? $add['title'] : date("Y-m-d H:i:s",$time);
		$add['classid']    = $this->classid;
		$add['titleurl']   = '/e/action/ShowInfo.php?classid='.$this->classid.'&id='.$add['id'];
		$add['newstime']   = $add['newstime'] ? to_time($add['newstime']) : $time;
		$add['truetime']   = $time;
		$add['lastdotime'] = $time;
		$add['havehtml']   = $add['havehtml'] ? int($add['havehtml']) : 0;
		$add['newspath']   = date("Y-m-d", $time);
		$add['userid']     = $add['userid'] ? $add['userid'] : '1';
		$add['username']   = $add['username'] ? $add['username'] : 'admin';
		// 索引表系统字段
		$ret_r=array();
		if($ecms == 2||$ecms == 3){
			$field = array('classid'=>0,'checked'=>1,'newstime'=>0,'truetime'=>0,'lastdotime'=>0,'havehtml'=>0);
			foreach($field as $f=>$v){
				$fval = isset($add[$f])?$add[$f]:$v;
				if($ecms==2)//添加
				{
					$ret_r[6].=",{$f}";
					$ret_r[7].=",'{$fval}'";
				}
				else//编辑
				{
					if($f=='truetime') continue;
					$ret_r[6].=",{$f}='{$fval}'";
				}
			}
			$ret_r[6] = substr($ret_r[6], 1);
			$ret_r[7] = substr($ret_r[7], 1);
			return $ret_r;
		}
		// 文件名
		if(!$add['filename']){
			//返回命名方式
			$_filename = $this->class_r[$this->classid]['filename'];
			if($_filename==1) //time命名
			{
				$filename=time().$add['id'];
			}
			elseif($_filename==2) //md5命名
			{
				$filename=md5(uniqid(microtime()));
			}
			elseif($_filename==3) //目录
			{
				$filename=$add['id'].'/index';
			}
			elseif($_filename==4) //date命名
			{
				$filename=date('Ymd').$add['id'];
			}
			elseif($_filename==5) //公共信息ID
			{
				$filename=ReturnInfoPubid($add['classid'],$add['id']);
			}
			else //id
			{
				$filename=$add['id'];
			}
			$add['filename'] = $this->class_r[$this->classid]['filename_qz'].$filename;
		}
		
		global $emod_r;
		// 主表系统字段
		$field[1] = array(
			'id'=>0,
			'classid'=>0,
			'ttid'=>0,
			'onclick'=>0,
			'plnum'=>0,
			'totaldown'=>0,
			'newspath'=>'',
			'filename'=>'',
			'userid'=>0,
			'username'=>'',
			'firsttitle'=>0,
			'isgood'=>'',
			'ispic'=>0,
			'istop'=>'',
			'isqf'=>0,
			'ismember'=>0,
			'isurl'=>0,
			'truetime'=>0,
			'lastdotime'=>0,
			'havehtml'=>0,
			'groupid'=>0,
			'userfen'=>0,
			'titlefont'=>'',
			'titleurl'=>'',
			'stb'=>$emod_r[$this->mid]['deftb'],
			'fstb'=>$this->public_r['filedeftb'], //存放附件表名	
			'restb'=>$this->public_r['pldeftb'], //存放评论表名
			'keyboard'=>'',
			'titlepic'=>''
		);
		// $ecms=0添加 |0主表字段 1主表值 2副表字段 3副表值
		// $ecms=1编辑 |0主表字段和值 3副表字段和值
		foreach($field[1] as $f=>$v){
			$fval = isset($add[$f]) ? $add[$f] : $v;
			if($f=='titleurl'){
			}else{
				$fval=ehtmlspecialchars($fval,ENT_QUOTES);
				$fval=str_replace("\r","",$fval);
				$fval=str_replace("\n","",$fval);
				$fval=RepPostStr2($fval);
			}

			if($ecms==0)//添加
			{
				$ret_r[0].=",{$f}";
				$ret_r[1].=",'{$fval}'";
			}
			else//编辑
			{
				$ret_r[0].=",{$f}='{$fval}'";
			}
		}
		// 副表系统字段
		$field[2] = array('id'=>0,'classid'=>0,'keyid'=>'','dokey'=>0,'newstempid'=>0,'closepl'=>0,'haveaddfen'=>0,'infotags'=>'');
		foreach($field[2] as $f=>$v){
			$fval = $add[$f] ? $add[$f] : $v;
			if($ecms==0)//添加
			{
				$ret_r[2].=",{$f}";
				$ret_r[3].=",'{$fval}'";
			}
			else//编辑
			{
				$ret_r[3].=",{$f}='{$fval}'";
			}
		}
		//检测必填字段
		$mustr=explode(",",$emod_r[$this->mid]['mustqenterf']); //必填项
		$mustcount=count($mustr)-1;
		for($i=1;$i<$mustcount;$i++)
		{
			if(!trim($mustr[$i]))
			{
				return $this->callback(100012);
				//printerror("EmptyQMustF","",1);
			}
		}
		//字段处理
		$fr = explode(',',$emod_r[$this->mid]['enter']); // qenter投稿项|enter录入项	
		$count = count($fr)-1;
		for($i=1;$i<$count;$i++)
		{
			$f=$fr[$i];
			// canaddf可增加项|caneditf可修改项	
			if($f=='special.field'||($ecms==0&&!strstr($emod_r[$this->mid]['canaddf'],','.$f.','))||($ecms==1&&!strstr($emod_r[$this->mid]['caneditf'],','.$f.',')))
			{continue;}
			if($f=='infoip')	 //ip
			{
				$fval=egetip();
			}
			elseif($f=='infoipport') //ip端口
			{
				$fval=egetipport();
			}
			elseif($f=='infozm')	//字母
			{
				$fval=$add[$f]?$add[$f]:GetInfoZm($add['title']);
			}
			else
			{
				$fval=ehtmlspecialchars($add[$f],ENT_QUOTES);
				$fval=str_replace("\n","<br />",$fval);
				$fval=RepPostStr2($fval);
			}
			
			// $isadd=$ecms==0?1:0;
			// $fval=DoFFun($this->mid,$f,$fval,$isadd,1);//执行函数
			$fval=addslashes($fval);
			if($ecms==0) // 添加
			{
				// 副表
				if(strstr($emod_r[$this->mid]['tbdataf'],','.$f.','))
				{
					$ret_r[2].=",".$f;
					$ret_r[3].=",'".$fval."'";
				}
				else
				{
					$ret_r[0].=",".$f;
					$ret_r[1].=",'".$fval."'";
				}
			}
			else // 编辑
			{
				// ip
				if($f=='infoip'||$f=='infoipport') continue;
				// 副表
				if(strstr($emod_r[$this->mid]['tbdataf'],','.$f.','))
				{
					$ret_r[3].=",".$f."='".$fval."'";
				}
				// 主表
				else
				{
					$ret_r[0].=",".$f."='".$fval."'";
				}
			}
		}
		$ret_r[0] = substr($ret_r[0], 1);
		$ret_r[1] = substr($ret_r[1], 1);
		$ret_r[2] = substr($ret_r[2], 1);
		$ret_r[3] = substr($ret_r[3], 1);
		// $ecms=0添加 |0主表字段 1主表值 2副表字段 3副表值
		// $ecms=1编辑 |0主表字段和值 3副表字段和值
		$ret_r[4]=$emod_r[$this->mid]['deftb'];
		return $ret_r;
	}

	
	/**
	 * 读取帝国CMS数据
	 * @param strint  $table    表名-->栏目ID-->栏目目录 (优先级同上)
	 * @param strint  $field    字段
	 * @param strint  $where    查询条件
	 * @param strint  $orderby  排序(id DESC)
	 * @param int     $limit    1读取单条数据，非1读取多条数据

	 * @return bool|array $r
	 * 
	 *  例(单条)：
	 *	1. 查询新闻表ID=99的数据
	 *	$api->infor('news', 'id,title', 'id=99');
	 * 
	 * 	2. 查询栏目目录old_news,ID=99的数据
	 * 	$api->infor('old_news', 'id,title', 'id=99');
	 * 
	 * 	3. 查询栏目ID=8,ID=99的数据
	 * 	$api->infor(8, 'id,title', 'id=99');
	 *
	 * 	4. 查询会员表，会员ID为66
	 * 	$api->infor('enewsmember', 'userid,username', 'userid=66');
	 *
	 *
	 *  例(多条)：
	 *	1. 查询新闻表最新10条数据
	 *	$api->inforOne('news', 'id,title', 'classid=1' ,'id DESC' ,10);
	 * 
	 * 	2. 查询栏目目录为old_news,的最新10条数据
	 * 	$api->infor('old_news', 'id,title', '' ,'id DESC' ,10);
	 * 
	 * 	3. 查询栏目ID=8的最新10条数据
	 * 	$api->infor(8, 'id,title', '' ,'id DESC' ,10);
	 *
	 * 	4. 查询会员表，会员组ID为1的最新10个会员
	 * 	$api->infor('enewsmember', 'userid,username', 'groupid=66' ,'userid DESC' ,10);
	 *
	 *  例:
	 *	返回json：$r = $api->callBackType('json')->infor('bid', 'id,title,classid', 'id=1');
	 *	返回json：$r = $api->callBackType('print')->infor('bid', 'id,title,classid', 'id=1');
	 *
	 */
	public function infor($table='', $field='*', $where='', $order='', $limit='1'){
		if(!$this->tbname($table)) return false;
		if(!$limit) return $this->callback(100005);
		if(!$where){
			if(!$this->classid) return $this->callback(100004);
			$where = "classid='{$this->classid}'";
		}
		//排序
		$order = $order ? "order by $order" : '';
		$sql = "select $field from {$this->table} where {$where} {$order} limit {$limit}";
		$this->SQL = $sql;

		if($limit==1){
			$data = $this->one($sql); //单查
			if(!$data)
				$data = array('count'=>0);
			else
				$data['count'] = 1;
		}else{
			$data = $this->query($sql); //多查
			$data = array('data'=>$data,'count'=>count($data));
		}
		return $this->callback(0, '', $data);

	}
	
	/**
	 * 格式化数据(去除数据键名数据)
	 * @param array $r 要处理的数据
	 * @param int $bqno 序号
	 * @return array
	 */
	private function formatData($r, $bqno=0){
		if(!$r) return $this->callback(100006);

		$res = array();
		foreach($r as $k=>$v){
			if(is_numeric($k)) continue;
			
			if($k=='newstime'||$k=='truetime'||$k=='lastdotime'){
				$res[$k.'YmdHis'] = date("Y-m-d H:i:s",$v);
				$res[$k.'Ymd'] = date("Y-m-d",$v);
			}else{
				$res[$k] = stripslashes($v);
			}
		}
		// 数据序号
		if($bqno) $res['bqno'] = $bqno;
		return $res;
	}
	
	/**
	 * 设置返回类型
	 * @param  string $type return=原样返回|json=输出json|print|var_dump
	 * @return $this
	 */
	public function callBackType($type){
		$this->printType = $type;
		return $this;
	}
	
	/**
	 * 打印数据(json_encode中文不转义需PHP>=5.4)
	 * @param arrty $arr
	 */	
	public function printData($arr){
		$type = $this->printType;
		if($type=='var_dump')   { echo '<pre>'; var_dump($arr); }
		else if($type=='print') { echo '<pre>'; print_r($arr); }
		else if($type=='json')  { echo json_encode($arr, JSON_UNESCAPED_UNICODE); }
		else { return $arr;}
	}
	
	/**
	 * error code
	 * @param int    $errCode 错误代码
	 * @param strint $errMsg  错误附加说明
	 * @param arrty  $arr     需要打印的数据
	 * @return bool
	 */
	private function callback($errCode=0, $errMsg='', $arr=array()) {
		$Msg = array(
            0 => 'success',
            100001 => '表名不能为空',
            100002 => '栏目ID不能为空',
            100003 => 'SQL语句不能为空',
            100004 => 'where条件不能为空',
            100005 => '需要指定查询条数',
            100006 => '暂时没有数据',
            100007 => '更新数据参数不能为空',
            100008 => '增加数据参数不能为空',
            100009 => '插入数据失败',
            100010 => '更新数据失败',
            100011 => '删除数据失败',
            100012 => '必填项无值',
            100013 => '信息ID不能为空',
            100014 => '信息ID错误',
            100015 => '管理信息失败'
		);
		$errMsg = $Msg[$errCode].($errMsg ? "($errMsg)" : '');
		$this->errCode = $errCode;
		$this->errMsg = $errMsg;
		
		$data = $arr;
		$data['errCode'] = $errCode;
		$data['errMsg']  = $errMsg;
		$this->printData($data);
		$this->printType='return'; //恢复回调方式
		return $errCode ? false : !empty($arr) ? $data : true;
	}
}